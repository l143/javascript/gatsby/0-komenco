import React from 'react'
import {Link} from "gatsby"

import Seo from "../components/seo"

function Gallery() {
  return (
    <>
    <Seo title="Galery" />
    <Link to="/">Go back to the homepage</Link>
    </>
  )
}

export default Gallery
