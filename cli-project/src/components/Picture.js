import React from 'react'

function Picture({ src }) {
  return (
    <div>
      <img src={src} alt="unsplash-picture" />
    </div>    
  )
}

export default Picture
