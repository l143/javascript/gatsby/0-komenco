# Gatsby 

> Escrito en marzo del 2022

Framework que ayuda a crear sitios web estáticos en base a contenidos manejados por CMS u otras fuentes.   

También cuenta con un servicio de construcción en nube para lograr un preview y hacer deploy.


> Gatsby is the fast and flexible framework that makes building websites with any CMS, API, or database fun again. Build and deploy headless websites that drive more traffic, convert better, and earn more revenue!

---

## Start

Para empezar un proyecto con gatsby tenemos 3 alternativas:
 - Instalarlo manualmente
 - Hacer uso cel cli
 - Usar un starter diferente al por defecto al del cli

> Proyecto manual

Se requieren de las dependencias:
- react
- react-dom
- gatsby

> Cli

Existe un cli que debe ser instalado globalmente para poder crear proyectos de forma más rádpida. Este cli usará un estarter que está deinido por defecto.

``` bash
$ npm install -g gatsby-cli
$ gatsby new <project-name> # Más automático 
$ gatsby new #Interactivo 
```

> Starters  

Hay algunos starters que podemos usar para proyectos específicos dentro de la siguiente [url](https://www.gatsbyjs.com/starters/)

```
$ gatsby new blog <Username>/<StarterName>
```

Ej.
```
$ gatsby new blog gastbyjs/gatsby-starter-blog
```


---
## Directorios
Dentro de src

> Pages  

Los archivos están relacionados a las rutas, por lo que un nuevo archivo generará una nueva ruta.

Ej.

```
/src
  - pages
    - index.js
    - mia.js
```

Creará las rutas

**http:localhost:8080/**  
**http:localhost:8080/mia**

## 

> Componentes  

Aqui están los componentes que podemos usar para mostrar el contenido.

--- 
## Archivos

> [gatsby-config.js](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-config/)    


*¿Qué es?*   

Es un archivo de configuración.

*¿Cuál es su objetivo?*  
- Permite agregar metadata del sitio.
- Permite agregar y configurar  [plugins](https://www.gatsbyjs.com/plugins) 
- Otras configuraciones generales.  
    - (Ej. DEV_WEBPACK_CACHE, LAZY_IMAGES, QUERY_ON_DEMAND, ...)
    - Puede ver las [configuraciones](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-config/#flags) y un poco del [código fuente](https://github.com/gatsbyjs/gatsby/blob/master/packages/gatsby/src/utils/flags.ts) para más información.

> [gatsby-browser.js](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-browser/)

*¿Qué es?*   
Es un archivo de JS que permite definir lógica para manejar eventos específicos en el navegador mediante el uso de la **Gatsby Browser API** y componentes de react.

*¿Cuál es su objetivo?*  

Que podamos aprovechar componentes globales que ayudan a responder a eventos específicos del navegador.

*Ejemplo*

wrapRootElement Permite definir un componente que funcionará de wrapper para todas las vistas del sitio.


> [gatsby-node](https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/)  

*¿Qué es?*   
Es un archivo de configuración que define acciones para el tiempo de **construcción**.

*¿Cuál es su objetivo?*  
Poder crear páginas dinámicas, agregar data a graphQl, o responder a eventos durante el **build** de gatsby.   


---

## Plugins 

Podría decirse que existen 3 tipos de plugins.
 - Componente
   - Resuelven una problemática particular 
 - Comportamiento / Transformación
   - Modifica o transforma de alguna forma el contenido.
   - Ejemplo: [react-helmet](https://www.gatsbyjs.com/plugins/gatsby-plugin-react-helmet/), [sharp](https://www.gatsbyjs.com/plugins/gatsby-transformer-sharp/)
 - Fuentes de datos
   - Nos ayuda a obtener la información de alguna fuentes (endpoints, cms, etc...) y llevarla a grpahql.
   - Ejemplo: [source-filesystem](https://www.gatsbyjs.com/plugins/gatsby-source-filesystem/)

## TO-DO
- Revisar despliegue manual
- Hablar con chepe
- Como se trigerean los builds